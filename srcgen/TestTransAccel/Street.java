package urbanizacja;


class Street   implements NumbersHaver
{
	private int	Numbers;
	private String	Name;
	public int	getNumbers() { return Numbers; }
	public void	setNumbers(int w) { Numbers = w; }
	public String	getName() { return Name; }
	public void	setName(String w) { Name = w; }
	public  boolean  isNumber () {
			return !Numbers.empty();
	}
	public  void  crossing () {
			Name = "skrzyżowanie";
	}
};
